import csv
import subprocess
import sys

count1 = 0
count0 = 0

file_out = open(sys.argv[2], "w")

def check_ech_for_domain(domain):
    # Execute the dig command
    try:
        result = subprocess.run(
                ["dig", domain, "TYPE65", "+short"],
                capture_output=True, text=True)
        return result.stdout
    except Exception as e:
        print(f"Failed to execute dig for {domain_name}: {str(e)}")
        return ""

def dig_req(file_path):
    global count1, count0, file_out
    file = open(file_path, "r")
    while True:
        line = file.readline()
        
        if not line:
            break
        
        domain_name =  line.split(",")[1].strip()
        output = check_ech_for_domain(domain_name)

        # Check if "ech" is in the output
        if "ech" in output:
            count1 += 1
            file_out.write(f"{domain_name},1\n")
            # Execute further commands here as needed
        else:
            count0 += 1
            file_out.write(f"{domain_name},0\n")

if __name__ == "__main__":

    file_path = sys.argv[1]
    dig_req(file_path)
    file_out.write(f"\n ** SUCEESS: {count1}, FAIL: {count0} **\n")
